using System.Collections.Generic;
using Codenames.Classes.Models;
using Microsoft.AspNetCore.Mvc;
using Codenames.Classes;

namespace Codenames.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private GameService service { get; }


        public GameController(GameService service) => this.service = service;

        [HttpGet]
        public IEnumerable<Game> GetGames()
        {
            return GameService.GetGames();
        }


        [HttpGet]
        [Route("get/{id}/")]
        public Game GetGame(int id)
        {
            return service.GetGame(id);
        }

        [HttpGet]
        [Route("{gameId}/checkCard/{id}/")]
        public GameStep CheckCard(int gameId, int id)
        {
            return service.CheckCard(gameId, id);
        }
    }
}
