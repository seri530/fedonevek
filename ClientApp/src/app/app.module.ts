import { AppComponent } from './app.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { CodenamesHomeComponent } from './codenames/codenames-home/codenames-home.component';
import { CodenamesModule } from './codenames/codenames.module';
import { HomeComponent } from './home/home.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AuthServiceConfig, GoogleLoginProvider, SocialLoginModule } from 'angularx-social-login';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("803599977476-ak381c02mn4sd2eumnoqh7r4j0nb7kjh.apps.googleusercontent.com")
  }
]);

const socialLoginConfig = () => config;

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'codenames/:id', component: CodenamesHomeComponent, pathMatch: 'full' },
    ]),
    CodenamesModule,
    AuthenticationModule,
    SocialLoginModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: socialLoginConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
