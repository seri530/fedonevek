import { CodenamesListComponent } from './codenames-list.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('CodenamesListComponent', () => {
  let component: CodenamesListComponent;
  let fixture: ComponentFixture<CodenamesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodenamesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodenamesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
