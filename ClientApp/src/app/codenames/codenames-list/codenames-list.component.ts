import { Game } from './../models/game';
import { CodenamesService } from '../services/codenames.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'codenames-list',
  templateUrl: './codenames-list.component.html',
  styleUrls: ['./codenames-list.component.css']
})
export class CodenamesListComponent implements OnInit {
  games: Game[];
  newGameId: number;

  constructor( private router: Router, private service: CodenamesService) { }

  ngOnInit() {
    this.service.startConnection();
    this.service.gameAdded.subscribe(
      _ => {
        this.newGameId = this.service.gameList.reduce(
          (a, b) => a.id > b.id ? a : b, { id: 0 }
        ).id + 1;
      });
  }

  ngOnDestroy() {
    this.service.gameAdded.unsubscribe();
  }
}
