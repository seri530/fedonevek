import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'codenames-home',
  templateUrl: './codenames-home.component.html',
  styleUrls: ['./codenames-home.component.css']
})
export class CodenamesHomeComponent implements OnInit {
  gameId: number;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        this.gameId = parseInt(params.get('id')) || 1;
      }
    );
  }
}
