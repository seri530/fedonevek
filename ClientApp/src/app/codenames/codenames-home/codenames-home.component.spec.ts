import { CodenamesHomeComponent } from './codenames-home.component';
import { BoardComponent } from '../board/board.component.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of } from 'rxjs';

const fakeActivatedRoute = {
  paramMap: of( convertToParamMap({id: 1})),
}

describe('CodenamesHomeComponent', () => {
  let component: CodenamesHomeComponent;
  let fixture: ComponentFixture<CodenamesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodenamesHomeComponent, BoardComponent],
      providers: [{ provide: ActivatedRoute, useFactory: () => fakeActivatedRoute }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodenamesHomeComponent);
    component = fixture.componentInstance;
    component.gameId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
