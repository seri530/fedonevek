import { BoardComponent } from './board/board.component';
import { CardComponent } from './card/card.component';
import { CodenamesHomeComponent } from './codenames-home/codenames-home.component';
import { CodenamesListComponent } from './codenames-list/codenames-list.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HubConnectionBuilder } from '@microsoft/signalr';

@NgModule({
  declarations: [
    BoardComponent,
    CardComponent,
    CodenamesHomeComponent,
    CodenamesListComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    CodenamesListComponent,
    CodenamesHomeComponent
  ],
  providers: [
    HubConnectionBuilder
  ]
})
export class CodenamesModule { }
