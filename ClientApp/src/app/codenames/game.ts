import { Game as GameDto } from './models/game';
import { GameInstruction } from './models/game-instruction';
import { GamePartial } from './models/game-partial';
import { GameStep } from './models/game-step';
import { Instruction } from './models/instruction';
import { InvalidStateError } from '../errors/invalid-state-error';
import { SocialUser } from 'angularx-social-login';

export class Game extends GameDto {
    users: SocialUser[];

    constructor(gameId: number) {
        super();
        this.id = gameId || -1;
        this.instruction = new Instruction();
        this.cards = [];
        this.isFinished = false;
        this.nextTeam = 0;
        this.sequenceNumber = 0;
        this.users = [];
    }

    addPartial(partial: GamePartial): Game {
        if (this.id !== partial.id || this.sequenceNumber !== partial.sequenceNumber - 1) {
            throw new InvalidStateError();
        }

        this.sequenceNumber = partial.sequenceNumber;
        if (partial['card']) {
            const step = partial as GameStep;
            if (this.nextTeam !== step.nextTeam && !step.isFinished) {
              this.instruction = new Instruction();
            }
            this.nextTeam = step.nextTeam;
            this.isFinished = step.isFinished;
            const cardId = this.cards.findIndex(c => c.id === step.card.id);
            this.cards[cardId] = step.card;
        }
        if (partial['instruction']){
          const instruction = partial as GameInstruction;
          this.instruction = instruction.instruction || new Instruction();
        }

        return this;
    }
}
