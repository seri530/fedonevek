import { CardComponent } from './card.component';
import { Card } from '../models/card';
import { CardState } from '../models/card-state.enum';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';


describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    component.data = CARD;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the name', () => {
    const title = fixture.debugElement.query(By.css('p'));
    expect(title.nativeElement.textContent).toBe("MY NAME");
  });

  describe('overlap card', () => {
    it('should not visible, if is not visited', () => {
      component.data.isVisited = true;
      component.data.state = CardState.Red;

      fixture.detectChanges();

      const overlap = fixture.debugElement.query(By.css('div'));
      expect(overlap.nativeElement.isVisible).toBeFalsy();
    });


    // it('should not visible, if State is Unknown', () => {
    //   const overlap = fixture.debugElement.query(By.css('div'));
    //   expect(overlap.nativeElement.isVisible).toBeFalsy();
    // });
  });
});

const CARD: Card = { id: 1, isVisited: false, name: 'MY NAME', state: CardState.Unknown };