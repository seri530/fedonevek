import { Card } from './../models/card';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'codenames-card',
    templateUrl: 'card.component.html'
})

export class CardComponent implements OnInit {
    @Input() data: Card;
    constructor() { }

    ngOnInit() { }
}
