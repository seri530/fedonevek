import { Game } from '../game';
import { EventEmitter, Output } from '@angular/core';

export class CodenamesService {

  @Output() public gameUpdate = new EventEmitter<Game>();
  @Output() public gameAdded = new EventEmitter<Game>();

  public gameList: Game[] = [];

  constructor() { }

  public startConnection(): Promise<any> {
    return Promise.resolve();
  }

  public async getGames(): Promise<Game[]> {
    return this.gameList;
  }

  public async getGame(gameId): Promise<Game> {
    return new Game(gameId);
  }

  public async activateCard(gameId: number, cardId: number) {
    console.log('CALLED');
    return Promise.resolve(null);
  }

  public async onGameUpdate(gamePartial) {
  }

  public onNewGame(game) {
  }
}
