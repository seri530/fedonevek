import { Game } from '../game';
import { GamePartial } from '../models/game-partial';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { SocialUser } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})

export class CodenamesService {
  private hubConnection: HubConnection;
  private getFromList = (gameId) => this.gameList.find(g => g.id === gameId);

  @Output() public gameUpdate = new EventEmitter<Game>();
  @Output() public gameAdded = new EventEmitter<Game>();

  public gameList: Game[] = [];

  constructor(private connectionBuilder: HubConnectionBuilder) { }

  public startConnection(): Promise<any> {
    if (this.hubConnection)
      return new Promise(() => { });

    this.hubConnection = this.connectionBuilder
      .withUrl('codenameshub')
      .withAutomaticReconnect([0,5000,10000,20000,100000, null])
      .build();

    this.attachListeners();

    return this.hubConnection
      .start()
      .then(_ => this.getGames());
  }

  public async getGames(): Promise<Game[]> {
    this.gameList = (await this.hubConnection.invoke("getgames")).map(g => Object.assign(new Game(0),g));
    this.gameAdded.next(this.gameList[0]);
    return this.gameList;
  }

  public async getGame(gameId): Promise<Game> {
    const game = this.gameList.find(g => g.id === gameId);
    return game || await this._loadGame(gameId);
  }

  public async _loadGame(gameId): Promise<Game>{
    const game = await this.hubConnection.invoke<Game>("getGame", gameId);
    this.gameList.push(game);
    return game;
  }

  public async activateCard(gameId: number, cardId: number) {
    const game = await this.getGame(gameId);
    if (game.isFinished)
      return;
    this.hubConnection.send("activateCard", gameId, cardId);
  }

  public async signInGame(gameId: number, user: SocialUser) {
    this.hubConnection.send("signInGame", gameId, user);
  }

  public async onGameUpdate(gamePartial) {
    try {
      await this.updateGameListWithPartial(gamePartial);
      this.gameUpdate.next(gamePartial);
    } catch (error) {
      this.gameList = await this.getGames();
      const game = this.getFromList(gamePartial.id);
      this.gameAdded.next(game);
    }
  }

  public onNewGame(game) {
    this.updateGameList(game);
    this.gameAdded.next(game);
  }

  private attachListeners() {
    this.hubConnection.on("newGame", (g) => this.onNewGame(g));
    this.hubConnection.on("gameUpdate", (g) => this.onGameUpdate(g));
  }

  private async updateGameListWithPartial(gamePartial: GamePartial): Promise<Game> {
    const game = await this.getGame(gamePartial.id);
    return game.addPartial(gamePartial);
  }

  private updateGameList(game: Game): void {
    const index = this.gameList.findIndex(g => g.id == game.id);
    if (index >= 0)
      this.gameList[index] = game;
    else
      this.gameList.push(game);
  }
}
