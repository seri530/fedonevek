import { CodenamesService } from './codenames.service';
import { Game } from '../game';
import { GameStep } from '../models/game-step';
import { Instruction } from '../models/instruction';
import { Team } from '../models/team.enum';
import { getTestBed, TestBed } from '@angular/core/testing';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { SocialUser } from 'angularx-social-login';

describe('CodenamesService', () => {
  let injector: TestBed;
  let service: CodenamesService;
  let hubCBuilder: HubConnectionBuilder;
  let game: Game;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CodenamesService, HubConnectionBuilder],
    });

    injector = getTestBed();
    service = injector.get(CodenamesService);
    hubCBuilder = injector.get(HubConnectionBuilder);

    game = Object.assign(new Game(2), { cards: [], isFinished: false, nextTeam: Team.Red, instruction: new Instruction(), sequenceNumber: 1 });

  });

  it('should be created', () => {
    const service: CodenamesService = TestBed.get(CodenamesService);
    expect(service).toBeTruthy();
  });

  it('should initialize the websocket', () => {
    const mock: any = new MockHubConnection();

    const service: CodenamesService = TestBed.get(CodenamesService);
    spyOn(hubCBuilder, 'withUrl').and.returnValue(hubCBuilder);
    spyOn(hubCBuilder, 'build').and.returnValue(mock);
    spyOn(mock, 'start').and.returnValue(Promise.resolve(null));

    service.startConnection();

    expect(hubCBuilder.withUrl).toHaveBeenCalled();
    expect(hubCBuilder.build).toHaveBeenCalled();
    expect(mock.start).toHaveBeenCalled();
  });

  describe('getGames', () => {

    let mockConnection: any;

    beforeEach(() => {
      mockConnection = new MockHubConnection();
      service['hubConnection'] = mockConnection;
    });

    it('should set games into service', async () => {
      const service: CodenamesService = TestBed.get(CodenamesService);
      spyOn(mockConnection, 'invoke').and.returnValue(GAMES);

      const games = await service.getGames();

      expect(games).toBeDefined();
      expect(games.length).toBe(GAMES.length);
      expect(service.gameList).toBeDefined();
      expect(service.gameList.length).toBe(GAMES.length);
    });
  });

  describe('getGame', () => {
    let mockConnection: any;

    beforeEach(() => {
      mockConnection = new MockHubConnection();
      service['hubConnection'] = mockConnection;
    });

    it('should get back the game', async () => {
      const service: CodenamesService = TestBed.get(CodenamesService);
      spyOn(mockConnection, 'invoke').and.returnValue(GAMES[0]);

      const game = await service.getGame(0);
      expect(mockConnection.invoke).toHaveBeenCalled();
      expect(game.id).toBe(1);
    });
  });

  describe('activeateCard', () => {
    it('should make a hub call', async () => {
      const mockConnection: any = new MockHubConnection();
      service['hubConnection'] = mockConnection;
      game.isFinished = false;
      service.gameList = [game];

      spyOn(mockConnection, 'send').and.returnValue(null);

      await service.activateCard(game.id, 0);

      expect(mockConnection.send).toHaveBeenCalledWith('activateCard', game.id, 0);
    });

    it('should not make a call, if the game is finished', async () => {
      const mockConnection: any = new MockHubConnection();
      service['hubConnection'] = mockConnection;

      game.isFinished = true;
      service.gameList = [game];

      spyOn(mockConnection, 'send').and.returnValue(null);

      await service.activateCard(game.id, 1);

      expect(mockConnection.send).not.toHaveBeenCalledWith('activateCard', game.id, 0);
    });
  });

  describe('hub message listeners', () => {
    describe('onGameUpdate', () => {
      it('game should be updated', async () => {
        const change = new GameStep();
        change.id = 2;
        change.nextTeam = Team.Blue;
        change.sequenceNumber = 2;
        service.gameList.push(game);

        await service.onGameUpdate(change);

        expect(service.gameList[0].sequenceNumber).toBe(change.sequenceNumber);
        expect(service.gameList[0].nextTeam).toBe(change.nextTeam);
      });

      it('game should be reloaded, if least one message was not arrived', async () => {
        const mockConnection: any = new MockHubConnection();
        service['hubConnection'] = mockConnection;

        const newGame = Object.assign({},GAMES[0],{sequenceNumber: 4});
        spyOn(mockConnection, 'invoke').and.returnValue([newGame]);

        const change = new GameStep();
        change.id = 2;
        change.nextTeam = Team.Blue;
        change.sequenceNumber = 3;
        service.gameList.push(game);

        await service.onGameUpdate(change);

        expect(mockConnection.invoke).toHaveBeenCalled();
        expect(service.gameList[0].sequenceNumber).toBe(newGame.sequenceNumber);
        expect(service.gameList[0].nextTeam).toBe(newGame.nextTeam);
    });
    });

    describe('onNewGame', () => {
      it('game list should be updated', () => {
        service.gameList = [];

        service.onNewGame(game);

        expect(service.gameList.length).toBe(1);
      });
    });


  });

  describe('signInGame', () => {
    let user;

    beforeEach(() => {
      user = new SocialUser();
    });

    it('should apply to game', async () => {
      const mockConnection: any = new MockHubConnection();
      service['hubConnection'] = mockConnection;

      spyOn(mockConnection, 'send').and.returnValue(null);

      await service.signInGame(game.id, user);

      expect(mockConnection.send).not.toHaveBeenCalledWith('activateCard', game.id, 0);
    });
  });
});

class MockHubConnection {
  start = () => { return Promise.resolve(); };
  on = () => null;
  invoke = () => { return Promise.resolve(GAMES); };
  send = () => null;
}

const GAMES: Game[] = [
  { id: 1, cards: [], isFinished: false, nextTeam: Team.Blue } as Game
]
