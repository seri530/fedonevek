import { Game } from '../game';
import { CodenamesService } from '../services/codenames.service';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';

@Component({
  selector: 'codenames-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  @Input() gameId: number;
  game: Game;

  constructor(private codenamesService: CodenamesService, private authService: AuthService) {
    this.game = new Game(0);
  }

  ngOnInit(): void {
    this.codenamesService.startConnection()
      .then(_ => this.updateGame());

    this.authService.authState
      .subscribe(user => this.signInGame(user));

    this.codenamesService.gameUpdate
      .subscribe(_ => this.updateGame());
  }

  async updateGame(): Promise<any> {
    this.game = await this.codenamesService.getGame(this.gameId);
  }

  checkCard(cardId: number): void {
    this.codenamesService.activateCard(this.game.id, cardId);
  }

  async signInGame(user): Promise<any> {
    await this.codenamesService.signInGame(this.gameId, user);
  }

  customTB(index, card) { return `${index}-${card.id}`; }
}
