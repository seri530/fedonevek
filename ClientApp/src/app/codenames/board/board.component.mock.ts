import { Game } from '../game';
import { CodenamesService } from '../services/codenames.service';
import { Component, Input, OnInit } from '@angular/core';



@Component({
  selector: 'codenames-board',
  template: '',
})
export class BoardComponent implements OnInit {
  @Input() gameId: number;
  game: Game;

  constructor() { 
  }

  ngOnInit(): void {}

  async updateGame(): Promise<any> { }

  checkCard(cardId: number): void { }
}
