import { BoardComponent } from './board.component';
import { CardComponent } from '../card/card.component';
import { CodenamesModule } from '../codenames.module';
import { Game } from '../game';
import { Card } from '../models/card';
import { CodenamesService } from '../services/codenames.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('CodenamesBoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;
  let service: CodenamesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [CodenamesModule],
      providers: [CodenamesService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    service = new CodenamesService();

    component.gameId = 1;
    component.game = Object.assign(new Game(1), { cards: CARDS });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the game getting service', async () => {
    const service = component['codenamesService'];
    spyOn(service, 'getGame').and.returnValue(Promise.resolve(null));
    component['codenamesService'] = service;

    await component.updateGame();
    expect(service.getGame).toHaveBeenCalled();
  });

  it('should call card checking', async () => {
    const service = component['codenamesService'];
    spyOn(service, 'activateCard');
    component['codenamesService'] = service;

    await component.checkCard(1);
    expect(service.activateCard).toHaveBeenCalled();
  });
});

const CARDS = [
  { id: 1, name: "", isVisited: false } as Card
];
