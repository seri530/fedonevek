import { LogInComponent } from './login-button/login-button.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    LogInComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LogInComponent
  ],
  // providers: [
  //   Authentication
  // ]
})
export class AuthenticationModule { }
