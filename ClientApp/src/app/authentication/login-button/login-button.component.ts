import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Subscription } from 'rxjs';

@Component({
  selector: 'login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.css']
})
export class LogInComponent {
  private authStateSubscription: Subscription;
  currentUser: SocialUser;
  loggedIn: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authStateSubscription = this.authService.authState.subscribe((user) => {
      this.currentUser = user;
      this.loggedIn = (user != null);
      console.log(user);
    });
  }

  ngOnDestroy(): void {
    this.authStateSubscription.unsubscribe();
  }

  signInGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }
}
