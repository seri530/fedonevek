using System.Threading.Tasks;
using Codenames.Classes.Models;
using Microsoft.AspNetCore.SignalR;

namespace Codenames.Classes
{
    public class CodenamesMessagingService
    {
        private IHubContext<CodenamesHub> hubcontext { get; set; }
        public CodenamesMessagingService(IHubContext<CodenamesHub> hubContext)
        {
            this.hubcontext = hubContext;
        }

        public async Task SendMessage(GameMessage message, GamePartial content)
        {
            await this.hubcontext.Clients.All.SendAsync(message.ToString(), content);
        }
    }

    public enum GameMessage
    {
        NewGame, Step, NewInstruction, RemoveInstruction, Join
    }
}