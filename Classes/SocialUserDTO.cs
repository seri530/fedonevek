using System;

namespace Codenames.Classes
{
    [Serializable]
    public class SocialUserDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string photoUrl { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string provider { get; set; }
    }
}