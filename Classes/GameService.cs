using System.Collections.Generic;
using Codenames.Classes.Models;

namespace Codenames.Classes
{
    public class GameService
    {
        protected static Dictionary<int, Game> Games { get; } = new Dictionary<int, Game>();
        private CodenamesMessagingService messagingService { get; }
        public GameService(CodenamesMessagingService messagingService) => this.messagingService = messagingService;

        public static IEnumerable<Game> GetGames()
        {
            return Games.Values;
        }
        
        public Game GetGame(int id)
        {
            Game game;

            Games.TryGetValue(id, out game);
            if (game == null)
            {
                game = new Game(id);
                Games.Add(game.Id, game);
                messagingService.SendMessage(GameMessage.NewGame, game).GetAwaiter().GetResult();
            }

            return game;
        }

        public GameStep CheckCard(int gameId, int id){
            Game game;
            
            Games.TryGetValue(gameId, out game);
            var step = game.GameStep(id);
            messagingService.SendMessage(GameMessage.Step, step).GetAwaiter().GetResult();

            return step;
        }

        public GameJoin signInGame(int gameId, SocialUserDTO user){
            Game game;

            Games.TryGetValue(gameId, out game);
            var join = game.signInGame(user);
            
            if (join == null)
                return null;

            messagingService.SendMessage(GameMessage.Join, join).GetAwaiter().GetResult();
            return join;
        }
    }
}