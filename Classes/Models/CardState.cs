using System;

namespace Codenames.Classes.Models
{
    [Serializable]
        public enum CardState
    {
        Unknown, Red, Blue, Spy, Pedestrian
    }
}
