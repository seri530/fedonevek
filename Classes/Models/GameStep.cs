using System;
using System.Text.Json.Serialization;

namespace Codenames.Classes.Models
{
    [Serializable]
        public class GameStep : GamePartial, IGameStep
    {
        public Card Card { get; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Team NextTeam { get; set; }

        public bool IsFinished { get; set; }

        public override int SequenceNumber => sequenceNumber;

        private int sequenceNumber;

        internal GameStep(Game game, Card card){
            this.Id = game.Id;
            this.sequenceNumber = game.GetSequenceNumber();
            this.Card = card;
        }
    }
}
