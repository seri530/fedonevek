namespace Codenames.Classes.Models
{
    public abstract class GamePartial
    {
        public int Id { get; protected set; }
        public abstract int SequenceNumber { get; }
    }
}
