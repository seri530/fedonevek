using System;
using System.Linq;
using Codenames.Classes.Helpers;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Codenames.Classes.Models
{

    [Serializable]
    public class Game : GamePartial, IGameStep, IGameInstruction
    {
        public Instruction Instruction { get; }
        public CardList Cards { get; internal set; } = null;
        public Team NextTeam { get; set; }
        public bool IsFinished { get; set; } = false;

        private int sequenceNumber = 0;
        public override int SequenceNumber { get { return this.sequenceNumber; } }

        public List<SocialUserDTO> Users { get; set; }

        public Game(int? id)
        {
            var rng = new Random();
            bool IsRedStart = rng.Next(2) == 0;

            Id = id ?? 0;
            Cards = GetRandomCards(IsRedStart);
            NextTeam = IsRedStart ? Team.Red : Team.Blue;
            Users = new List<SocialUserDTO>();
        }

        internal int GetSequenceNumber()
        {
            return ++this.sequenceNumber;
        }

        private Team GetNextTeam()
        {
            return this.NextTeam == Team.Blue ? Team.Red : Team.Blue;
        }

        public GameStep GameStep(int cardId)
        {
            if (this.IsFinished)
                return null;

            var selected = this.Cards.GetCard(cardId);
            selected.isVisited = true;

            var step = new GameStep(this, selected);

            NextTeam = step.NextTeam =
                selected.state.ToString() == NextTeam.ToString() ? NextTeam : GetNextTeam();
            this.IsFinished = this.calculateIsFinished();

            return step;
        }

        public GameJoin signInGame(SocialUserDTO user)
        {
            if (this.Users.Find(u => u.Id == user.Id) == null) {
                this.Users.Add(user);
                return new GameJoin(this, user);
            }
            return null;
        }

        private bool calculateIsFinished()
        {
            var statistic = this.Cards
                .GroupBy(c => c.endState)
                .Select(g => new { Key = g.Key, Count = new { Visited = g.Count(c => c.isVisited), NotVisited = g.Count(c => !c.isVisited) } })
                .ToDictionary(g => g.Key, g => g.Count);

            return statistic[CardState.Spy].Visited > 0 || statistic[CardState.Blue].NotVisited == 0 || statistic[CardState.Red].NotVisited == 0;
        }

        private CardList GetRandomCards(bool IsRedStart)
        {
            var cardNames = GetRandomCardNames();
            var cards = new CardList();

            cards.AddRange(cardNames.Take(9).Select(name => new Card(name, IsRedStart ? CardState.Red : CardState.Blue)));
            cards.AddRange(cardNames.Skip(9).Take(8).Select(name => new Card(name, IsRedStart ? CardState.Blue : CardState.Red)));
            cards.AddRange(cardNames.Skip(17).Take(7).Select(name => new Card(name, CardState.Pedestrian)));
            cards.Add(new Card(cardNames.Last(), CardState.Spy));

            cards.Shuffle();
            cards.Renumber();
            return cards;
        }

        private IList<string> GetRandomCardNames()
        {
            var randomList = CardNames;
            randomList.Shuffle();
            return randomList.Take(25).ToList();
        }

        string Serialize(Card card){
            return "";
        }

        static internal IList<string> CardNames = new List<string>(){
            "kesztyű","banán","játék","atlantisz","arc","fog","hajó","kéz","idegen","kenyér","fok","gitár","függ","könnyű","bolt","gyémánt","fal","gyöngy","amazon","ár","kemény","kárpátok","haza","ordít","határ","kés","banda","jézus","asztal","jeti","figura","hajó","ketchup","húr","kentaur","fut","gumi","fű","korona","bomba","gerinc","fáklya","gyökér","áru","anglia","kenguru","kártya","hideg","fekete","himalája","hotel","bálna","király","jogász","betegség","harang","kalóz","ág","bíró","cérna","adó","hold","caesar","fűszer","gáz","erdő","bánya","kor","búvár","kereszt","elem","anya","csiga","csempész","ég","hullám","bot","kód","jár","bika","hatalom","távolság","agy","birka","chaplin","afrika","hollywood","cápa","fül","gazda","einstein","barát","korbács","boszorkány","kereszt","ejtőernyő","anyag","csillag","csavar","édes","csirke","india","fej","angyal","erő","bank","csap","iskola","gyűrű","kaszinó","furulya","fa","körte","egér","csokoládé","fúj","benzin","autó","kölyök","amerika","jegy","koporsó","baba","féreg","kocka","helikopter","csatorna","denevér","alpok","dob","cső","képernyő","forma","egyszervú","atom","béka","gomb","hét","alföld","hó","fagylalt","dél","ajtó","kám","kar","hajt","homok","álom","kaktusz","háló","csizma","idő","sivatag","apró","éjszaka","báb","cirkusz","indián","hóember","katona","felhő","európa","kórház","egészség","csomó","föld","bőr","azték","kormény","árnyék","jég","koncert","berlin","fedél","kutya","hercegnő","cipő","dió","arany","duna","csöpp","kerek","forrás","egyenes","ausztrália","beethoven","görög","híd","alak","háború","élet","daru","ágy","kel","kard","hal","sajt","alma","kacsacsőrű","halál"};
    }
}
