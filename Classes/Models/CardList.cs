using System.Collections.Generic;
using System.Linq;

namespace Codenames.Classes.Models
{
    public class CardList : List<Card>
    {
        public void Renumber()
        {
            int newId = 0;
            this.ForEach(c => c.id = newId++);
        }

        public Card GetCard(int id)
        {
            return this.FirstOrDefault(c => c.id == id);
        }
    }
}