namespace Codenames.Classes.Models
{
    public class GameJoin : GamePartial
    {
        public override int SequenceNumber { get { return this.sequenceNumber; } }
        private int sequenceNumber { get; set; }

        public SocialUserDTO User { get; set; }

        internal GameJoin(Game game, SocialUserDTO user)
        {
            User = user;
            this.sequenceNumber = game.GetSequenceNumber();
        }
    }
}
