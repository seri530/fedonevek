using System;
using System.Text.Json.Serialization;

namespace Codenames.Classes.Models
{
    [Serializable]
        public class Card
    {
        public int id { get; internal set; }
        public string name { get; }
        public bool isVisited { get; set; }

        [JsonPropertyName("state")]
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public CardState state
        {
            get
            {
                return this.isVisited ? this.endState : CardState.Unknown;
            }
        }

        internal CardState endState { get; }

        public Card(string name, CardState state)
        {
            this.id = 0;
            this.name = name;
            this.endState = state;
        }
    }
}
