namespace Codenames.Classes.Models
{
    public interface IGameStep
    {
        public Team NextTeam { get; set; }
        public bool IsFinished { get; set; }
    }
}
