using System;

namespace Codenames.Classes.Models
{
    [Serializable]
    public class Instruction
    {
        public string keyword { get; }
        public int connectionCount { get; }
        public Instruction(string keyword, int connectionCount)
        {
            this.keyword = keyword;
            this.connectionCount = connectionCount;
        }
    }
}
