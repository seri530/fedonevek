namespace Codenames.Classes.Models
{
    public class GameInstruction : GamePartial, IGameInstruction
    {
        public Instruction Instruction { get; }

        public override int SequenceNumber { get; }
    }
}