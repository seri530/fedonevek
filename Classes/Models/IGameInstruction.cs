namespace Codenames.Classes.Models
{
    public interface IGameInstruction
    {
        public Instruction Instruction { get; }
    }
}
