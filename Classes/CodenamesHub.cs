﻿using Codenames.Classes.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace Codenames.Classes
{
    public class CodenamesHub : Hub
    {
        private IHubContext<CodenamesHub> _context { get; }
        private GameService service { get; set; }

        public CodenamesHub(GameService service)
        : base()
        {
            this.service = service;
        }

        public override async Task OnConnectedAsync()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, "SignalR Users");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "SignalR Users");
            await base.OnDisconnectedAsync(exception);
        }

        public async Task BroadcastGame(Game game)
        {
            await Clients?.All.SendAsync("newGame", game);
        }
        public async Task Broadcast(GamePartial partial)
        {
            await Clients?.All.SendAsync("gameUpdate", partial);
        }

        public IEnumerable<Game> GetGames()
        {
            return GameService.GetGames();
        }

        [HubMethodName("activateCard")]
        public async void ActivateCardASync(int gameId, int cardId)
        {
            var step = service.CheckCard(gameId, cardId);
            await this.Broadcast(step);
        }

        [HubMethodName("signInGame")]
        public async void signInGame(int gameId, SocialUserDTO user)
        {
            var step = service.signInGame(gameId, user);
            await this.Broadcast(step);
        }

        public Game GetGame(int gameId)
        {
            return service.GetGame(gameId);
        }
    }
}
