using System;
using System.Reflection;
using TypeScriptModelsGenerator;
using TypeScriptModelsGenerator.Definitions;
using Codenames.Classes.Models;
using Codenames.Classes;

namespace Codenames
{
    public static class TypescriptGenerator
    {
        private const string destination = "ClientApp/src/app/codenames";

        public static void Build()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var definition = new TypeScriptDefinition();

            definition.IncludeEnum<CardState>();
            definition.IncludeEnum<Team>();

            definition.Include<GamePartial>();
            definition.Include<GameInstruction>();
            definition.Include<Card>();
            definition.Include<GameStep>();
            definition.Include<Instruction>();

            var gameDef = definition.Include<Game>();
            gameDef
                .Property(x => x.Cards, o => {
                    o.Ignore();
                })
                .Property(x => x.Users, o =>{
                    o.Ignore();
                })

                .AddProperty(typeof(Card[]), "cards", false);

            TypeScriptModelsGeneration.Setup(definition, destination, options => {
                options.AddNamespaceReplaceRule("Codenames.Classes", String.Empty);
                options.AddTypeReplaceRule("CardList","Card[]");
            }).Execute();
        }
    }
}
